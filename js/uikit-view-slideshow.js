(function ($) {

  'use strict';

  /**
   * Attaches the behavior to bootstrap carousel view.
   */
  Drupal.behaviors.uikit_view_slideshow = {
    attach: function (context, settings) {
      var addCover = ["img", "video", "iframe"];
      $.each(addCover, function (index, value) {
        var cover = $('.uk-slideshow-items li').find(value);
        if (cover.length > 0) {
          cover.attr("uk-cover", '');
        }
      });
    }
  }
}(jQuery));
