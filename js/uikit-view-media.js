(function ($) {

  'use strict';

  /**
   * Attaches the behavior to bootstrap carousel view.
   */
  Drupal.behaviors.uikit_view_media = {
    attach: function (context, settings) {
      $('.uk-card-media-left, .uk-card-media-right').each(function () {
        if ($(this).find('img').length === 1) {
          $(this).find('img').attr("uk-cover", '');
          if ($(this).find('canvas').length === 1) {
            $(this).find('canvas').attr({
              "width": $(this).width(),
              "height": $(this).height()
            });
          }
        }
      });
    }
  }
}(jQuery));
