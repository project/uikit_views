<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit Grid component.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_grid",
 *   title = @Translation("UIkit Grid"),
 *   help = @Translation("Displays rows in a UIkit Grid component"),
 *   theme = "uikit_view_grid",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewGrid extends UIkitViewDefaultStyle {

  /**
   * Does the style plugin support flex of rows.
   *
   * @var bool
   */
  protected $flex = TRUE;

  /**
   * Does the style plugin support scrollspy of rows.
   *
   * @var bool
   */
  protected $scrollspy = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['uikit_grid'] = [
      'default' => [
        'width_@s' => 'uk-child-width-1-1@s',
        'width_@m' => 'uk-child-width-1-2@m',
        'width_@l' => 'uk-child-width-1-3@l',
        'width_@xl' => 'uk-child-width-1-4@xl',
        'divider' => FALSE,
        'match_height' => FALSE,
        'gutter' => '',
        'row' => '',
        'column' => '',
        'masonry' => FALSE,
        'parallax' => '',
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $breakpoints = [
      '@s' => $this->t('Affects device widths of 480px and higher.'),
      '@m' => $this->t('Affects device widths of 768px and higher.'),
      '@l' => $this->t('Affects device widths of 960px and higher.'),
      '@xl' => $this->t('Affects device widths of 1220px and higher.'),
    ];

    $form['uikit_grid'] = [
      '#type' => 'details',
      '#title' => $this->t('Grid options'),
      '#open' => TRUE,
      '#weight' => 3,
      '#description' => $this->t("To create a grid whose child elements' widths are evenly split, we apply one class to the grid for each breakpoint. Each item in the grid is then automatically applied a width based on the number of columns selected for each breakpoint. See <a href='@href' target='_blank' title='@title'>Grid component</a> for more details.", [
        '@href' => 'https://getuikit.com/docs/grid',
        '@title' => 'Grid component - UIkit documentation',
      ]),
    ];

    foreach (['@s', '@m', '@l', '@xl'] as $size) {
      $form['uikit_grid']["width_${size}"] = [
        '#type' => 'select',
        '#title' => $this->t("uk-child-width-*${size}"),
        '#default_value' => $this->options['uikit_grid']["width_${size}"],
        '#options' => [
          ' ' => $this->t('- None -'),
          "uk-child-width-1-1${size}" => 1,
          "uk-child-width-1-2${size}" => 2,
          "uk-child-width-1-3${size}" => 3,
          "uk-child-width-1-4${size}" => 4,
          "uk-child-width-1-5${size}" => 5,
          "uk-child-width-1-6${size}" => 6,
          "uk-child-width-1-10${size}" => 10,
        ],
        '#description' => $breakpoints[$size],
      ];
    }

    $form['uikit_grid']['divider'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grid divider'),
      '#default_value' => $this->options['uikit_grid']['divider'],
      '#description' => $this->t('Apply a horizontal border to each row in the grid, except the first row.'),
    ];

    $form['uikit_grid']['match_height'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Match height'),
      '#default_value' => $this->options['uikit_grid']['match_height'],
      '#description' => $this->t('To match the height of the direct child of each cell.'),
    ];

    $form['uikit_grid']['gutter'] = [
      '#type' => 'select',
      '#title' => $this->t('Grid gutter'),
      '#default_value' => $this->options['uikit_grid']['gutter'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-grid-small' => $this->t('Small gutter'),
        'uk-grid-medium' => $this->t('Medium gutter'),
        'uk-grid-large' => $this->t('Large gutter'),
        'uk-grid-collapse' => $this->t('Collapse gutter'),
      ],
      '#description' => $this->t('Grids automatically create a horizontal gutter between columns and a vertical one between two succeeding grids. By default, the grid gutter is wider on large screens.<br /><strong>Note</strong>: <em class="placeholder">Grid collapse</em> removes the grid gutter.'),
    ];

    $form['uikit_grid']['row'] = [
      '#title' => t('Row'),
      '#type' => 'select',
      '#default_value' => $this->options['uikit_grid']['row'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-grid-row-small' => $this->t('apply a small gap to the row.'),
        'uk-grid-row-medium' => $this->t('apply a medium gap to the row.'),
        'uk-grid-row-large' => $this->t('apply a large gap to the row.'),
        'uk-grid-row-collapse' => $this->t('apply a collapse gap to the row.'),
      ],
    ];

    $form['uikit_grid']['column'] = [
      '#title' => t('Column'),
      '#type' => 'select',
      '#default_value' => $this->options['uikit_grid']['column'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-grid-column-small' => $this->t('apply a small gap to the column.'),
        'uk-grid-column-medium' => $this->t('apply a medium gap to the column.'),
        'uk-grid-column-large' => $this->t('apply a large gap to the column.'),
        'uk-grid-column-collapse' => $this->t('apply a collapse gap to the column.'),
      ],
    ];

    $form['uikit_grid']['masonry'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Masonry'),
      '#default_value' => $this->options['uikit_grid']['masonry'],
    ];

    $form['uikit_grid']['parallax'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parallax'),
      '#default_value' => $this->options['uikit_grid']['parallax'],
      '#size' => 60,
      '#maxlength' => 128,
    ];
  }
}
