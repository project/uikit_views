<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit Transition.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_transition",
 *   title = @Translation("UIkit Transition"),
 *   help = @Translation("Displays rows in a UIkit transition component"),
 *   theme = "uikit_view_transition",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewTransition extends UIkitViewGrid {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['transition'] = [
      'default' => [
        'thumbnail' => NULL,
        'thumbnail_second' => NULL,
        'effects' => 'uk-transition-fade',
        'overlay' => NULL,
        'position' => NULL,
        'icon' => NULL,
        'transition_margin' => NULL,
        'inverse' => NULL,
        'content_class' => 'uk-flex uk-flex-center uk-flex-middle',
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['transition'] = [
      '#type' => 'details',
      '#title' => $this->t('Transition options'),
      '#open' => TRUE,
      '#weight' => 1,
      '#description' => $this->t("Create a responsive transition gallery with images and videos. See <a href='@href' target='_blank' title='@title'>transition component</a> for more details.", [
        '@href' => 'https://getuikit.com/docs/transition',
        '@title' => 'Transition component - UIkit documentation',
      ]),
    ];

    $form['transition']['thumbnail'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnail image'),
      '#options' => $this->displayHandler->getFieldLabels(TRUE),
      '#default_value' => $this->options['transition']['thumbnail'],
      '#required' => TRUE,
    ];

    $form['transition']['thumbnail_second'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnail image second'),
      '#options' => $this->displayHandler->getFieldLabels(TRUE),
      '#default_value' => $this->options['transition']['thumbnail_second'],
    ];

    $form['transition']['effects'] = [
      '#type' => 'select',
      '#title' => $this->t('effects'),
      '#required' => TRUE,
      '#options' => [
        'uk-transition-fade' => $this->t('Lets the element fade in'),
        'uk-transition-scale-up' => $this->t('The element scales up'),
        'uk-transition-scale-down' => $this->t('The element scales down'),
        'uk-transition-slide-top' => $this->t('Top'),
        'uk-transition-slide-top-small' => $this->t('Top: smaller'),
        'uk-transition-slide-top-medium' => $this->t('Top: medium'),
        'uk-transition-slide-bottom' => $this->t('Bottom'),
        'uk-transition-slide-bottom-small' => $this->t('Bottom: smaller'),
        'uk-transition-slide-bottom-medium' => $this->t('Bottom: medium'),
        'uk-transition-slide-left' => $this->t('Left'),
        'uk-transition-slide-left-small' => $this->t('Left: smaller'),
        'uk-transition-slide-left-medium' => $this->t('Left: medium'),
        'uk-transition-slide-right' => $this->t('Right'),
        'uk-transition-slide-right-small' => $this->t('Right: smaller'),
        'uk-transition-slide-right-medium' => $this->t('Right: medium'),
      ],
      '#default_value' => $this->options['transition']['effects'],
    ];
    $form['transition']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => [
        '' => $this->t('- None -'),
        'uk-position-top' => $this->t('Top'),
        'uk-position-left' => $this->t('Lefto'),
        'uk-position-right' => $this->t('Right'),
        'uk-position-bottom' => $this->t('Bottom'),
        'uk-position-cover' => $this->t('Cover'),
      ],
      '#default_value' => $this->options['transition']['position'],
      '#description' => $this->t('To toggle a transition on hover or focus.'),
    ];

    $form['transition']['transition_margin'] = [
      '#type' => 'select',
      '#title' => $this->t('Position margin'),
      '#default_value' => $this->options['transition']['transition_margin'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-position-small' => $this->t('Small'),
        'uk-position-medium' => $this->t('Medium'),
        'uk-position-large' => $this->t('Large'),
      ],
    ];

    $form['transition']['overlay'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay style modifiers'),
      '#default_value' => $this->options['transition']['overlay'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-overlay-default' => $this->t('Default'),
        'uk-overlay-primary' => $this->t('Primary'),
      ],
    ];

    $form['transition']['icon'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Icon'),
      '#default_value' => $this->options['transition']['icon'],
    ];

    $form['transition']['inverse'] = [
      '#type' => 'select',
      '#title' => $this->t('Inverse'),
      '#default_value' => $this->options['transition']['inverse'] ? $this->options['transition']['inverse'] : '',
      '#description' => $this->t('Inverse the style of any component for light or dark backgrounds.'),
      '#options' => [
        '' => $this->t('- None -'),
        'uk-light' => 'Light',
        'uk-dark' => 'Dark',
      ],
    ];

    $form['transition']['content_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content additional class'),
      '#default_value' => $this->options['transition']['content_class'],
      '#description' => $this->t('Classes are separated by spaces.'),
      '#maxlength' => 255,
    ];
  }
}
