<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\Table;

/**
 * Style plugin to render each item in a UIkit Table component.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_table",
 *   title = @Translation("UIkit Table"),
 *   help = @Translation("Displays rows in a UIkit Table component"),
 *   theme = "uikit_view_table",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewTable extends Table {

  protected function defineOptions() {
    $options = parent::defineOptions();

    // UIkit table view options.
    $options['overflow_auto'] = ['default' => FALSE];
    $options['table_modifiers'] = ['default' => []];
    $options['table_size'] = ['default' => ''];
    $options['header_field'] = ['default' => []];

    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $args = [
      '@href' => 'https://getuikit.com/docs/table',
      '@title' => 'Table component - UIkit documentation',
    ];

    $fields = $this->displayHandler->getFieldLabels(TRUE);

    $form['uikit_table_options'] = [
      '#type' => 'item',
      '#title' => $this->t('UIkit table options'),
      '#description' => $this->t('Easily create nicely looking tables, which come in different styles. See <a href="@href" target="_blank" title="@title">Table component</a> for more details.', $args),
    ];
    $form['overflow_auto'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('overflow auto'),
      '#description' => $this->t('If your table happens to be wider than its container element or would eventually get too big on a specific viewport width, this creates a container that provides a horizontal scrollbar whenever the elements inside it are wider than the container itself.'),
      '#default_value' => $this->options['overflow_auto'],
    ];

    $form['table_modifiers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Table modifiers'),
      '#description' => $this->t('Display the table in a different style. These modifiers can be used together.'),
      '#default_value' => $this->options['table_modifiers'],
      '#options' => [
        'uk-table-divider' => $this->t('Divider modifier'),
        'uk-table-striped' => $this->t('Striped modifier'),
        'uk-table-hover' => $this->t('Hover modifier'),
        'uk-table-justify' => $this->t('Justify modifier'),
        'uk-table-middle' => $this->t('Alignment modifier'),
        'uk-table-responsive' => $this->t('Stack on small viewports'),
      ],
    ];

    $form['table_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size modifiers'),
      '#description' => $this->t('Display the table in a different style. These modifiers can be used together.'),
      '#default_value' => $this->options['table_size'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-table-small' => $this->t('Small'),
        'uk-table-large' => $this->t('Large'),
      ],
    ];

    foreach ($fields as $key => $field_mame) {
      $form["header_field"][$key] = [
        '#type' => 'textfield',
        '#title' => $this->t("Header ${field_mame} class"),
        '#default_value' => isset($this->options["header_field"][$key]) ? $this->options["header_field"][$key] : '',
        '#maxlength' => 255,
      ];
    }
  }

}
