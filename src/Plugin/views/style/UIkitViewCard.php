<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit card component.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_card",
 *   title = @Translation("UIkit card"),
 *   help = @Translation("Displays in a UIkit card component"),
 *   theme = "uikit_view_card",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewCard extends UIkitViewGrid {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['cards'] = [
      'default' => [
        'type' => 'card',
        'card' => [
          'footer' => [],
          'badge' => NULL,
        ],
        'media' => [
          'position' => '',
        ],
        'header' => [],
        'style' => '',
        'hover' => TRUE,
        'size' => '',
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    unset($form['row_class']);
    $form['cards'] = [
      '#type' => 'details',
      '#title' => $this->t('Card options'),
      '#open' => TRUE,
      '#weight' => 1,
      '#description' => $this->t("Create layout boxes with different styles. See <a href='@href' target='_blank' title='@title'>card component</a> for more details.", [
        '@href' => 'https://getuikit.com/docs/card',
        '@title' => 'Card component - UIkit documentation',
      ]),
    ];

    $form['cards']['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#default_value' => $this->options['cards']['type'],
      '#options' => [
        'card' => $this->t('Card'),
        'media' => $this->t('Media'),
      ],
      '#required' => TRUE,
    ];

    $form['cards']['header'] = [
      '#type' => 'select',
      '#title' => $this->t('Header'),
      '#options' => ['' => $this->t('--None--')] + $this->displayHandler->getFieldLabels(TRUE),
      '#default_value' => $this->options['cards']['header'],
    ];

    $form['cards']['card'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="style_options[cards][type]"]' => ['value' => 'card'],
        ],
      ],
    ];

    $form['cards']['card']['footer'] = [
      '#type' => 'select',
      '#title' => $this->t('footer'),
      '#options' => ['' => $this->t('--None--')] + $this->displayHandler->getFieldLabels(TRUE),
      '#default_value' => $this->options['cards']['card']['footer'],
    ];

    $form['cards']['card']['badge'] = [
      '#type' => 'select',
      '#title' => $this->t('Badge'),
      '#options' => ['' => $this->t('--None--')] + $this->displayHandler->getFieldLabels(TRUE),
      '#default_value' => $this->options['cards']['card']['badge'],
    ];

    $form['cards']['media'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="style_options[cards][type]"]' => ['value' => 'media'],
        ],
      ],
    ];

    $form['cards']['media']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Media Position'),
      '#default_value' => $this->options['cards']['media']['position'],
      '#options' => [
        '' => $this->t('- None -'),
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
    ];

    $form['cards']['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Style modifiers'),
      '#default_value' => $this->options['cards']['style'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-card-default' => $this->t('Add this class to create a visually styled box.'),
        'uk-card-primary' => $this->t('Add this class to modify the card and emphasize it with a primary color.'),
        'uk-card-secondary' => $this->t('Add this class to modify the card and give it a secondary background color.'),
      ],
    ];

    $form['cards']['hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hover modifier'),
      '#default_value' => $this->options['cards']['hover'],
    ];

    $form['cards']['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size modifiers'),
      '#default_value' => $this->options['cards']['size'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-card-small' => $this->t('Add this class to apply a smaller padding'),
        'uk-card-large' => $this->t('Add this class to apply a larger padding.'),
      ],
    ];
  }

}
