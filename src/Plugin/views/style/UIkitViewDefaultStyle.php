<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\uikit_views\UIkitViews;

/**
 * Unformatted style plugin to render rows one after another with no
 * decorations.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_default",
 *   title = @Translation("UIKit view default style"),
 *   help = @Translation("Displays rows one after another."),
 *   theme = "views_view_unformatted",
 *   display_types = {"summary"}
 * )
 */
class UIkitViewDefaultStyle extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * Does the style plugin support animation of rows.
   *
   * @var bool
   */
  protected $animation = FALSE;

  /**
   * Does the style plugin support flex of rows.
   *
   * @var bool
   */
  protected $flex = FALSE;

  /**
   * Does the style plugin support scrollspy of rows.
   *
   * @var bool
   */
  protected $scrollspy = FALSE;

  /**
   * Get the raw field value.
   *
   * @param $index
   *   The index count of the row.
   * @param $field
   *   The id of the field.
   */

  public function getFieldEntity($index, $field) {
    $this->view->row_index = $index;
    $value = $this->view->field[$field]->getEntity($this->view->result[$index]);
    unset($this->view->row_index);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['classes'] = ['default' => ''];

    if ($this->scrollspy()) {
      $options['scrollspy'] = [
        'default' => [
          'cls' => '',
          'hidden' => FALSE,
          'offset-top' => 0,
          'offset-left' => 0,
          'repeat' => FALSE,
          'delay' => 500,
          'target' => '> div',
          'toggle' => FALSE,
        ],
      ];
    }

    if ($this->flex()) {
      $options['flex'] = [
        'default' => [
          'horizontal' => '',
          'vertical' => '',
        ],
      ];
    }

    if ($this->animation()) {
      $options['animation'] = [
        'default' => [
          'action' => '',
          'toggle' => FALSE,
          'reverse' => FALSE,
          'fast' => FALSE,
          'origin' => '',
          'position' => 'views',
        ],
      ];
    }

    return $options;
  }

  /**
   * Returns the scrollspy property.
   *
   * @return bool
   */
  public function scrollspy() {
    return $this->scrollspy;
  }

  /**
   * Returns the animation property.
   *
   * @return bool
   */
  public function animation() {
    return $this->animation;
  }

  /**
   * Returns the animation property.
   *
   * @return bool
   */
  public function flex() {
    return $this->flex;
  }

  public function animationOptions() {
    $animation_options = [
      '' => $this->t('- None -'),
      'uk-animation-fade' => $this->t('Fade'),
      'uk-animation-scale-up' => $this->t('Scale Up'),
      'uk-animation-scale-down' => $this->t('Scale Down'),
      'uk-animation-slide-top' => $this->t('Top'),
      'uk-animation-slide-top-small' => $this->t('Top small'),
      'uk-animation-slide-top-medium' => $this->t('Top medium'),
      'uk-animation-slide-bottom' => $this->t('Bottom'),
      'uk-animation-slide-bottom-small' => $this->t('Bottom small'),
      'uk-animation-slide-bottom-medium' => $this->t('Bottom medium'),
      'uk-animation-slide-left' => $this->t('Left'),
      'uk-animation-slide-left-small' => $this->t('Left small'),
      'uk-animation-slide-left-medium' => $this->t('Left medium'),
      'uk-animation-slide-right' => $this->t('Right'),
      'uk-animation-slide-right-small' => $this->t('Right small'),
      'uk-animation-slide-right-medium' => $this->t('Right medium'),
      'uk-animation-shake' => $this->t('Shake'),
      'uk-animation-rand' => $this->t('Random'),
    ];

    return $animation_options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    unset($form['grouping']);

    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS class(es)'),
      '#default_value' => $this->options['classes'],
      '#maxlength' => 255,
    ];

    if ($this->flex()) {
      $form['flex'] = [
        '#type' => 'details',
        '#title' => $this->t('Flex options'),
        '#open' => TRUE,
        '#weight' => 4,
        '#description' => $this->t("Utilize the power of flexbox to create a wide range of layouts. See <a href='@href' target='_blank' title='@title'>Grid component</a> for more details.", [
          '@href' => 'https://getuikit.com/docs/flex',
          '@title' => 'Flex component - UIkit documentation',
        ]),
      ];
      $form['flex']['horizontal'] = [
        '#title' => t('Horizontal alignment'),
        '#type' => 'select',
        '#default_value' => $this->options['flex']['horizontal'],
        '#options' => [
          '' => $this->t('- None -'),
          'uk-flex-left' => $this->t('Add this class to align flex items to the left.'),
          'uk-flex-center' => $this->t('Add this class to center flex items along the main axis.'),
          'uk-flex-right' => $this->t('Add this class to align flex items to the right.'),
          'uk-flex-between' => $this->t('Add this class to distribute items evenly, with equal space between the items along the main axis.'),
          'uk-flex-around' => $this->t('Add this class to distribute items evenly with equal space on both sides of each item.'),
        ],
      ];
      $form['flex']['vertical'] = [
        '#title' => t('Vertical alignment'),
        '#type' => 'select',
        '#default_value' => $this->options['flex']['vertical'],
        '#options' => [
          '' => $this->t('- None -'),
          'uk-flex-stretch' => $this->t('Add this class to expand flex items to fill the height of their parent.'),
          'uk-flex-top' => $this->t('Add this class to align flex items to the top.'),
          'uk-flex-middle' => $this->t('Add this class to center flex items along the cross axis.'),
          'uk-flex-bottom' => $this->t('Add this class to align flex items to the bottom.'),
        ],
      ];
    }

    if ($this->animation()) {
      $form['animation'] = [
        '#type' => 'details',
        '#title' => t('Animation'),
        '#open' => TRUE,
        '#weight' => 5,
        '#description' => $this->t("A collection of smooth animations to use within your page. See <a href='@href' target='_blank' title='@title'>Grid component</a> for more details.", [
          '@href' => 'https://getuikit.com/docs/animation',
          '@title' => 'Animation component - UIkit documentation',
        ]),
      ];

      $form['animation']['position'] = [
        '#type' => 'select',
        '#title' => $this->t('Display position'),
        '#default_value' => $this->options['animation']['position'],
        '#options' => [
          'views' => $this->t('Views'),
          'row' => $this->t('Row'),
        ],
      ];

      $form['animation']['toggle'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Toggle'),
        '#default_value' => $this->options['animation']['toggle'],
        '#states' => [
          'visible' => [
            ':input[name="style_options[animation][position]"]' => ['value' => 'row'],
          ],
        ],
      ];

      $form['animation']['action'] = [
        '#type' => 'select',
        '#title' => $this->t('Action'),
        '#default_value' => $this->options['animation']['action'],
        '#options' => $this->animationOptions(),
      ];
      $form['animation']['reverse'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Reverse modifier'),
        '#default_value' => $this->options['animation']['reverse'],
      ];
      $form['animation']['fast'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Fast modifier'),
        '#default_value' => $this->options['animation']['fast'],
      ];
      $form['animation']['origin'] = [
        '#type' => 'select',
        '#title' => $this->t('Origin modifiers'),
        '#default_value' => $this->options['animation']['origin'],
        '#options' => [
          '' => $this->t('- None -'),
          'uk-transform-origin-top-left' => $this->t('Top left'),
          'uk-transform-origin-top-center' => $this->t('Top center'),
          'uk-transform-origin-top-right' => $this->t('Top right'),
          'uk-transform-origin-center-left' => $this->t('Left center'),
          'uk-transform-origin-center-right' => $this->t('Right center'),
          'uk-transform-origin-bottom-left' => $this->t('Bottom left.'),
          'uk-transform-origin-bottom-center' => $this->t('Bottom center'),
          'uk-transform-origin-bottom-right' => $this->t('Bottom right'),
        ],
      ];
    }

    if ($this->scrollspy()) {
      $form['scrollspy'] = [
        '#type' => 'details',
        '#title' => t('Scrollspy'),
        '#open' => TRUE,
        '#weight' => 6,
        '#description' => $this->t("Trigger events and animations while scrolling your page. See <a href='@href' target='_blank' title='@title'>Grid component</a> for more details.", [
          '@href' => 'https://getuikit.com/docs/scrollspy',
          '@title' => 'Scrollspy component - UIkit documentation',
        ]),
      ];
      $form['scrollspy']['cls'] = [
        '#type' => 'select',
        '#title' => $this->t('Action'),
        '#default_value' => $this->options['scrollspy']['cls'],
        '#options' => $this->animationOptions(),
      ];
      $form['scrollspy']['target'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Groups'),
        '#default_value' => $this->options['scrollspy']['target'],
        '#maxlength' => 255,
      ];
      $form['scrollspy']['hidden'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('hidden'),
        '#default_value' => $this->options['scrollspy']['hidden'],
      ];
      $form['scrollspy']['offset-top'] = [
        '#type' => 'number',
        '#title' => $this->t('offset top'),
        '#default_value' => $this->options['scrollspy']['offset-top'],
      ];
      $form['scrollspy']['offset-left'] = [
        '#type' => 'number',
        '#title' => $this->t('offset left'),
        '#default_value' => $this->options['scrollspy']['offset-left'],
      ];
      $form['scrollspy']['repeat'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Repeat'),
        '#default_value' => $this->options['scrollspy']['repeat'],
      ];
      $form['scrollspy']['delay'] = [
        '#type' => 'number',
        '#title' => $this->t('delay'),
        '#default_value' => $this->options['scrollspy']['delay'],
      ];
      $form['scrollspy']['toggle'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Toggle'),
        '#default_value' => $this->options['scrollspy']['toggle'],
      ];
    }

  }

}
