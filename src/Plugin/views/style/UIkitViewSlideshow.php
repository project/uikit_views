<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit slideshow.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_slideshow",
 *   title = @Translation("UIkit slideshow"),
 *   help = @Translation("Displays rows in a UIkit slideshow component"),
 *   theme = "uikit_view_slideshow",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewslideshow extends UIkitViewDefaultStyle {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['slideshow'] = [
      'default' => [
        'animation' => 'slide',
        'autoplay' => FALSE,
        'autoplay_interval' => 7000,
        'dotnav' => '',
        'draggable' => TRUE,
        'easing' => 'ease',
        'finite' => FALSE,
        'pause_on_hover' => TRUE,
        'ratio' => '16:9',
        'slidenav' => '',
        'inverse' => 'uk-dark',
        'index' => 0,
        'velocity' => 1,
        'min_height' => '',
        'max_height' => '',
        'thumbnav' => [
          'field' => '',
          'position' => 'uk-position-bottom-center',
          'margin' => 'uk-position-small',
          'vertical' => '',
        ],
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['slideshow'] = [
      '#type' => 'details',
      '#title' => $this->t('slideshow options'),
      '#open' => TRUE,
      '#description' => $this->t("Create a responsive slideshow with images and videos. See <a href='@href' target='_blank' title='@title'>Slideshow component</a> for more details.", [
        '@href' => 'https://getuikit.com/docs/slideshow',
        '@title' => 'Slideshow component - UIkit documentation',
      ]),
    ];

    $form['slideshow']['animation'] = [
      '#type' => 'select',
      '#title' => $this->t('Slideshow animation mode'),
      '#default_value' => $this->options['slideshow']['animation'],
      '#options' => [
        'slide' => $this->t('Slides slide in side by side'),
        'fade' => $this->t('Slides fade in'),
        'scale' => $this->t('Slides are scaled up and fade out'),
        'pull' => $this->t('Slides are pulled from the deck'),
        'push' => $this->t('Slides are pushed to the deck'),
      ],
    ];

    $form['slideshow']['slidenav'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Slidenav list'),
      '#default_value' => $this->options['slideshow']['slidenav'],
      '#options' => [
        'uk-slidenav-large' => $this->t('Large modifier'),
        'uk-hidden-hover' => $this->t('only appears on hover'),
      ],
    ];

    $form['slideshow']['inverse'] = [
      '#type' => 'select',
      '#title' => $this->t('Slidenav inverse'),
      '#options' => [
        'uk-light' => $this->t('Light'),
        'uk-dark' => $this->t('Dark'),
      ],
      '#default_value' => $this->options['slideshow']['inverse'],
    ];

    $form['slideshow']['dotnav'] = [
      '#type' => 'select',
      '#title' => $this->t('Dotnav list'),
      '#default_value' => $this->options['slideshow']['dotnav'],
      '#options' => [
        '' => $this->t('- None -'),
        'dotnav' => $this->t('Default'),
        'overlay' => $this->t('Position as overlay'),
        'thumbnav' => $this->t('Thumbnav'),
      ],
    ];

    $form['slideshow']['thumbnav'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="style_options[slideshow][dotnav]"]' => ['value' => 'thumbnav'],
        ],
      ],
    ];

    $form['slideshow']['thumbnav']['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnav field'),
      '#options' => $this->displayHandler->getFieldLabels(TRUE),
      '#required' => TRUE,
      '#default_value' => $this->options['slideshow']['thumbnav']['field'],
    ];

    $form['slideshow']['thumbnav']['margin'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnav margin'),
      '#default_value' => $this->options['slideshow']['thumbnav']['margin'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-position-small' => $this->t('Small'),
        'uk-position-medium' => $this->t('Medium'),
        'uk-position-large' => $this->t('Large'),
      ],
    ];

    $form['slideshow']['thumbnav']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnav position'),
      '#default_value' => $this->options['slideshow']['thumbnav']['position'],
      '#options' => [
        'uk-position-top-left' => $this->t('Positions the element at the top left'),
        'uk-position-top-center' => $this->t('Positions the element at the top center'),
        'uk-position-top-right' => $this->t('Positions the element at the top right'),
        'uk-position-center' => $this->t('Positions the element vertically centered in the middle'),
        'uk-position-center-left' => $this->t('Positions the element vertically centered on the left'),
        'uk-position-center-right' => $this->t('Positions the element vertically centered on the right'),
        'uk-position-bottom-left' => $this->t('Positions the element at the bottom left'),
        'uk-position-bottom-center' => $this->t('Positions the element at the bottom center'),
        'uk-position-bottom-right' => $this->t('Positions the element at the bottom right'),
      ],
    ];

    $form['slideshow']['thumbnav']['vertical'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Thumbnav vertical'),
      '#description' => $this->t('Enabled when thumb navigation position is left or right'),
      '#default_value' => $this->options['slideshow']['thumbnav']['vertical'],
    ];

    $form['slideshow']['pause_on_hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause autoplay mode on hover'),
      '#default_value' => $this->options['slideshow']['pause_on_hover'],
    ];

    $form['slideshow']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Slideshow autoplays'),
      '#default_value' => $this->options['slideshow']['autoplay'],
    ];

    $form['slideshow']['autoplay_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('The delay between switching slides in autoplay mode'),
      '#default_value' => $this->options['slideshow']['autoplay_interval'],
      '#size' => 60,
    ];

    $form['slideshow']['index'] = [
      '#type' => 'number',
      '#title' => $this->t('Slideshow item to show. 0 based index'),
      '#default_value' => $this->options['slideshow']['autoplay'],
      '#size' => 60,
    ];

    $form['slideshow']['velocity'] = [
      '#type' => 'number',
      '#title' => $this->t('The animation velocity (pixel/ms)'),
      '#default_value' => $this->options['slideshow']['velocity'],
      '#size' => 60,
    ];

    $form['slideshow']['finite'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable infinite sliding'),
      '#default_value' => $this->options['slideshow']['finite'],
    ];

    $form['slideshow']['ratio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ratio'),
      '#default_value' => $this->options['slideshow']['ratio'],
      '#maxlength' => 255,
    ];

    $form['slideshow']['min_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Min height'),
      '#default_value' => $this->options['slideshow']['min_height'],
      '#size' => 60,
    ];

    $form['slideshow']['max_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Max height'),
      '#default_value' => $this->options['slideshow']['max_height'],
      '#size' => 60,
    ];

    $form['slideshow']['draggable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Draggable.'),
      '#default_value' => $this->options['slideshow']['draggable'],
    ];

    $form['slideshow']['easing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('The animation easing (CSS timing functions or cubic-bezier)'),
      '#default_value' => $this->options['slideshow']['easing'],
    ];

  }

}
