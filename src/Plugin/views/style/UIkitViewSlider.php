<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit Slider.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_slider",
 *   title = @Translation("UIkit Slider"),
 *   help = @Translation("Displays rows in a UIkit Slider component"),
 *   theme = "uikit_view_slider",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewslider extends UIkitViewCard {

  /**
   * Does the style plugin support flex of rows.
   *
   * @var bool
   */
  protected $flex = FALSE;

  /**
   * Does the style plugin support scrollspy of rows.
   *
   * @var bool
   */
  protected $scrollspy = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['slider'] = [
      'default' => [
        'center' => FALSE,
        'autoplay' => FALSE,
        'autoplay_interval' => 7000,
        'finite' => FALSE,
        'sets' => FALSE,
        'navigation' => NULL,
        'dotnav' => TRUE,
        'inverse' => 'uk-dark',
        'pause_on_hover' => TRUE,
        'draggable' => TRUE,
        'index' => 0,
        'velocity' => 1,
        'easing' => 'ease',
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['slider'] = [
      '#type' => 'details',
      '#title' => $this->t('Slider options'),
      '#open' => TRUE,
      '#description' => $this->t("Create a responsive carousel slider. See <a href='@href' target='_blank' title='@title'>slider component</a> for more details.", [
        '@href' => 'https://getuikit.com/docs/slider',
        '@title' => 'Slider component - UIkit documentation',
      ]),
    ];


    $form['slider']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autoplay.'),
      '#default_value' => $this->options['slider']['autoplay'],
    ];

    $form['slider']['autoplay_interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autoplay interval.'),
      '#default_value' => $this->options['slider']['autoplay_interval'],
      '#states' => [
        'visible' => [
          ':input[name="style_options[slider][autoplay]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['slider']['finite'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Infinite Scrolling.'),
      '#default_value' => $this->options['slider']['finite'],
    ];
    $form['slider']['sets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Slide Sets.'),
      '#default_value' => $this->options['slider']['sets'],
    ];
    $form['slider']['draggable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Draggable.'),
      '#default_value' => $this->options['slider']['draggable'],
    ];
    $form['slider']['index'] = [
      '#type' => 'number',
      '#title' => $this->t('Index'),
      '#default_value' => $this->options['slider']['index'],
    ];
    $form['slider']['velocity'] = [
      '#type' => 'number',
      '#title' => $this->t('Velocity.'),
      '#default_value' => $this->options['slider']['velocity'],
    ];
    $form['slider']['navigation'] = [
      '#type' => 'select',
      '#title' => $this->t('Navigation'),
      '#default_value' => $this->options['slider']['navigation'] ? $this->options['slider']['navigation'] : 'enable',
      '#description' => $this->t('Navigation'),
      '#options' => [
        '' => '- None -',
        'default' => 'Default',
        'outside' => 'Navigation outside',
      ],
    ];
    $form['slider']['inverse'] = [
      '#type' => 'select',
      '#title' => $this->t('Inverse'),
      '#default_value' => $this->options['slider']['inverse'] ? $this->options['slider']['inverse'] : '',
      '#description' => $this->t('Inverse the style of any component for light or dark backgrounds.'),
      '#options' => [
        'uk-light' => 'Light',
        'uk-dark' => 'Dark',
      ],
    ];
    $form['slider']['dotnav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dotnav'),
      '#default_value' => $this->options['slider']['dotnav'],
    ];
    $form['slider']['pause_on_hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause on hover.'),
      '#default_value' => $this->options['slider']['pause_on_hover'],
    ];
    $form['slider']['center'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Center.'),
      '#default_value' => $this->options['slider']['center'],
    ];
    $form['slider']['easing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('The animation easing (CSS timing functions or cubic-bezier)'),
      '#default_value' => $this->options['slider']['easing'],
    ];
  }

}
