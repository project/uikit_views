<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render each item in a UIkit Filter component.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_filter",
 *   title = @Translation("UIkit Filter"),
 *   help = @Translation("Displays rows in a UIkit Filter component"),
 *   theme = "uikit_view_filter",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewFilter extends UIkitViewCard {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['filter'] = [
      'default' => [
        'target' => '.js-filter',
        'control' => '',
      ],
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['filter'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter options'),
      '#open' => TRUE,
    ];

    $form['filter']['control'] = [
      '#type' => 'select',
      '#title' => $this->t('Control field'),
      '#options' => $this->displayHandler->getFieldLabels(TRUE),
      '#required' => TRUE,
      '#default_value' => $this->options['filter']['control'],
      '#description' => $this->t('Select the field to use as the accordian title to create a toggle for the filter items.'),
    ];

    $form['filter']['target'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS selector of the element(s) to toggle.'),
      '#default_value' => $this->options['filter']['target'],
    ];

  }

}
