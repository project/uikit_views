<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit Slider.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_lightbox",
 *   title = @Translation("UIkit Lightbox"),
 *   help = @Translation("Displays rows in a UIkit lightbox component"),
 *   theme = "uikit_view_lightbox",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewLightbox extends UIkitViewGrid {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['lightbox'] = [
      'default' => [
        'original' => NULL,
        'thumbnail' => NULL,
        'type' => NULL,
        'caption' => NULL,
        'animation' => '',
        'autoplay' => 0,
        'autoplay_interval' => 0,
        'pause_on_hover' => FALSE,
        'toggle' => 'a',
        'index' => 0,
        'video_autoplay' => FALSE,
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['lightbox'] = [
      '#type' => 'details',
      '#title' => $this->t('Lightbox options'),
      '#open' => TRUE,
      '#weight' => 1,
      '#description' => $this->t("Create a responsive lightbox gallery with images and videos. See <a href='@href' target='_blank' title='@title'>Lightbox component</a> for more details.", [
        '@href' => 'https://getuikit.com/docs/lightbox',
        '@title' => 'Lightbox component - UIkit documentation',
      ]),
    ];
    $lightbox_options = ['' => $this->t('--None--')] + $this->displayHandler->getFieldLabels(TRUE);
    $form['lightbox']['original'] = [
      '#type' => 'select',
      '#title' => $this->t('Original image'),
      '#options' => $this->displayHandler->getFieldLabels(TRUE),
      '#required' => TRUE,
      '#default_value' => $this->options['lightbox']['original'],
      '#description' => $this->t('The Lightbox uses the href attribute to figure out the type of the linked content.'),
    ];

    $form['lightbox']['thumbnail'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnail image'),
      '#options' => $this->displayHandler->getFieldLabels(TRUE),
      '#default_value' => $this->options['lightbox']['thumbnail'],
      '#description' => $this->t('Lightbox Thumbnails View.'),
    ];

    $form['lightbox']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('content type'),
      '#options' => [
        '' => $this->t('- None -'),
        'image' => $this->t('The content type is an image'),
        'video' => $this->t('The content type is a video'),
        'iframe' => $this->t('The content type is a regular website'),
      ],
      '#default_value' => $this->options['lightbox']['type'],
      '#description' => $this->t("If no filename extension is defined in the path, just add the data-type attribute to the \<a\> tag."),
    ];
    $form['lightbox']['caption'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption'),
      '#options' => $lightbox_options,
      '#default_value' => $this->options['lightbox']['caption'],
      '#description' => $this->t('To display a caption at the bottom of the lightbox, set the data-caption attribute on an anchor.'),
    ];
    $form['lightbox']['toggle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Toggle selector'),
      '#default_value' => $this->options['lightbox']['toggle'],
      '#maxlength' => 255,
    ];
    $form['lightbox']['animation'] = [
      '#type' => 'select',
      '#title' => $this->t('animation'),
      '#default_value' => $this->options['lightbox']['animation'],
      '#options' => [
        '' => $this->t('- None -'),
        'slide' => $this->t('Slide'),
        'fade' => $this->t('Fade'),
        'scale' => $this->t('Scale'),
      ],
    ];
    $form['lightbox']['autoplay'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autoplay'),
      '#default_value' => $this->options['lightbox']['autoplay'],
      '#maxlength' => 255,
    ];
    $form['lightbox']['autoplay_interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autoplay interval'),
      '#default_value' => $this->options['lightbox']['autoplay_interval'],
      '#maxlength' => 255,
    ];
    $form['lightbox']['index'] = [
      '#type' => 'number',
      '#title' => $this->t('Index'),
      '#default_value' => $this->options['lightbox']['index'],
    ];
    $form['lightbox']['pause_on_hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause on hover.'),
      '#default_value' => $this->options['lightbox']['pause_on_hover'],
    ];
    $form['lightbox']['video_autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lightbox videos autoplay'),
      '#default_value' => $this->options['lightbox']['video_autoplay'],
    ];

  }

}
