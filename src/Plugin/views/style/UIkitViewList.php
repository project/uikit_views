<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit List component.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_list",
 *   title = @Translation("UIkit List"),
 *   help = @Translation("Displays rows in a UIkit List component"),
 *   theme = "uikit_view_list",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewList extends UIkitViewDefaultStyle {

  /**
   * Does the style plugin support animation of rows.
   *
   * @var bool
   */
  protected $animation = TRUE;

  /**
   * Does the style plugin support scrollspy of rows.
   *
   * @var bool
   */
  protected $scrollspy = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['list'] = ['default' => []];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    unset($form['animation']['toggle']);
    $form['animation']['position'] = [
      '#type' => 'value',
      '#default_value' => 'views',
    ];

    $args = [
      '@href' => 'https://getuikit.com/docs/list',
      '@title' => 'List component - UIkit documentation',
    ];

    $form['list'] = [
      '#title' => $this->t('List options'),
      '#description' => $this->t('The modifier to apply to the list. See <a href="@href" target="_blank" title="@title">List component</a> to view examples of each list modifier.', $args),
      '#type' => 'checkboxes',
      '#default_value' => $this->options['list'],
      '#options' => [
        'uk-list-divider' => $this->t('List divider'),
        'uk-list-striped' => $this->t('List striped'),
        'uk-list-large' => $this->t('List large'),
        'uk-list-bullet' => $this->t('Bullet modifier'),
      ],
    ];
  }

}
