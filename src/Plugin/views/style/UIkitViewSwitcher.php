<?php

namespace Drupal\uikit_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item in a UIkit switcher component.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "uikit_view_switcher",
 *   title = @Translation("UIkit switcher"),
 *   help = @Translation("Displays rows in a UIkit switcher component"),
 *   theme = "uikit_view_switcher",
 *   display_types = {"normal"}
 * )
 */
class UIkitViewswitcher extends UIkitViewDefaultStyle {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['switcher'] = [
      'default' => [
        'switcher_title' => NULL,
        'nav' => 'tab',
        'subnav' => [
          'type' => 'uk-subnav-pill',
        ],
        'tab' => [
          'position' => '',
        ],
        'alignment' => '',
        'connect' => '',
        'toggle' => '',
        'active' => 0,
        'animation' => '',
        'duration' => '',
        'swiping' => TRUE,
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['switcher'] = [
      '#type' => 'details',
      '#title' => t('Switcher'),
      '#open' => TRUE,
      '#description' => $this->t("Dynamically transition through different content panes. See <a href='@href' target='_blank' title='@title'>Switcher component</a> for more details.", [
        '@href' => 'https://getuikit.com/docs/switcher',
        '@title' => 'Switcher component - UIkit documentation',
      ]),
    ];

    $form['switcher']['switcher_title'] = [
      '#type' => 'select',
      '#title' => $this->t('Switch navigation fields'),
      '#options' => $this->displayHandler->getFieldLabels(TRUE),
      '#required' => TRUE,
      '#default_value' => $this->options['switcher']['switcher_title'],
    ];

    $form['switcher']['nav'] = [
      '#type' => 'radios',
      '#options' => [
        'tab' => $this->t('Tab'),
        'subnav' => $this->t('Subnav'),
      ],
      '#title' => $this->t('Switcher navigation type'),
      '#default_value' => $this->options['switcher']['nav'],
      '#required' => TRUE,
    ];

    $form['switcher']['tab'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="style_options[switcher][nav]"]' => ['value' => 'tab'],
        ],
      ],
    ];

    $form['switcher']['tab']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Tab frame Modifier'),
      '#default_value' => $this->options['switcher']['tab']['position'],
      '#options' => [
        '' => $this->t('- None -'),
        'uk-tab-bottom' => $this->t('Tab bottom'),
        'uk-tab-left' => $this->t('Tab left'),
        'uk-tab-right' => $this->t('Tab right'),
      ],
    ];

    $form['switcher']['subnav'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="style_options[switcher][nav]"]' => ['value' => 'subnav'],
        ],
      ],
    ];
    $form['switcher']['subnav']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Subnav type'),
      '#default_value' => $this->options['switcher']['subnav']['type'],
      '#options' => [
        'uk-subnav-divider' => $this->t('Divider modifier'),
        'uk-subnav-pill' => $this->t('Pill modifier'),
      ],
    ];
    $form['switcher']['alignment'] = [
      '#type' => 'select',
      '#title' => $this->t('Alignment'),
      '#default_value' => $this->options['switcher']['alignment'],
      '#options' => [
        '' => $this->t('Left alignment'),
        'uk-flex-center' => $this->t('Center alignment'),
        'uk-flex-right' => $this->t('Right alignment'),
        'uk-child-width-expand' => $this->t('equal distribution'),
        'uk-flex-around' => $this->t('Pill modifier'),
      ],
    ];
    $form['switcher']['connect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Related items container'),
      '#default_value' => $this->options['switcher']['connect'],
    ];
    $form['switcher']['toggle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trigger content switching'),
      '#default_value' => $this->options['switcher']['toggle'],
    ];

    $form['switcher']['active'] = [
      '#type' => 'number',
      '#title' => $this->t('Active index on init'),
      '#default_value' => $this->options['switcher']['active'],
    ];
    $form['switcher']['animation'] = [
      '#type' => 'select',
      '#title' => $this->t('Animations'),
      '#default_value' => $this->options['switcher']['animation'],
      '#multiple' => TRUE,
      '#options' => $this->animationOptions(),
    ];

    $form['switcher']['duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Animation duration'),
      '#default_value' => $this->options['switcher']['duration'],
    ];
    $form['switcher']['swiping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use swiping'),
      '#default_value' => $this->options['switcher']['swiping'],
      '#size' => 60,
    ];
  }

}
