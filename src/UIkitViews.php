<?php

namespace Drupal\uikit_views;

use Drupal\Component\Utility\Html;
use Drupal\views\ViewExecutable;

/**
 * The primary class for the Views Bootstrap module.
 *
 * Provides many helper methods.
 *
 * @ingroup utility
 */
class UIkitViews {

  /**
   * Returns the theme hook definition information for UIkit Views.
   */
  public static function getThemeHooks() {
    $hooks['uikit_view_accordion'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_accordion',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];
    $hooks['uikit_view_card'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_card',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];
    $hooks['uikit_view_grid'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_grid',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];
    $hooks['uikit_view_list'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_list',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];
    $hooks['uikit_view_slider'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_slider',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];

    $hooks['uikit_view_slideshow'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_slideshow',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];

    $hooks['uikit_view_lightbox'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_lightbox',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];

    $hooks['uikit_view_table'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_table',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];

    $hooks['uikit_view_filter'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_filter',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];

    $hooks['uikit_view_switcher'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_switcher',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];

    $hooks['uikit_view_transition'] = [
      'preprocess functions' => [
        'template_preprocess_uikit_view_transition',
      ],
      'file' => 'includes/uikit_views.theme.inc',
    ];

    return $hooks;
  }

  /**
   * Get unique element id.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A ViewExecutable object.
   *
   * @return string
   *   A unique id for an HTML element.
   */
  public static function getUniqueId(ViewExecutable $view) {
    $id = $view->storage->id() . '-' . $view->current_display;
    return Html::getUniqueId('uikit-views-' . $id);
  }
}
