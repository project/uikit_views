CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Using Views
* Maintainers

INTRODUCTION
------------

The Views UIKit module adds styles to Views to output the results of a view as several common UIKit components.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/uikit_views

DESCRIPTION
----------------
UIkit Views allows you to output by the UIkit components fastly, The module provides a Views 3 style plugin.
<h3>Supports</h3>
<ul>
  <li>Accordion</li>
  <li>Card</li>
  <li>Filter</li>
  <li>Grid</li>
  <li>Lightbox</li>
  <li>List</li>
  <li>Slider</li>
  <li>Slidershow</li>
  <li>Switcher</li>
  <li>Table</li>
  <li>Transition</li>
</ul>

<h3>Requirements</h3>
<ul>
  <li>Views</li>
  <li><a href="https://www.drupal.org/project/uikit" target="_blank" title="Uikit">Uikit</a> theme</li>
</ul>
<h3>Documentation</h3>
<ul>
  <li>UIKit website documentation： <a href="https://getuikit.com/docs/introduction" target="_blank" title="View documents">View documents</a></li>
</ul>

REQUIREMENTS
------------

This module needs the following UIKit topics or manual introduction of UIKit Library:

* Views (Core)
* UIKit Theme (https://www.drupal.org/project/uikit)
* UIKit Library (https://getuikit.com)

INSTALLATION
------------

* Install the Views UIKit module as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.

USING VIEWS
-----------

    1. Add style type and select which UIKit component style you wish to use
    2. Change the settings for that specific component

MAINTAINERS
-----------

* Xiang.gao - https://www.drupal.org/u/qiutuo

