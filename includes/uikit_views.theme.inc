<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

use Drupal\Core\Template\Attribute;
use Drupal\uikit_views\UIkitViews;

/**
 * Prepares variables for UIkit Default templates.
 *
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: The view object.
 *   - rows: An array of row items. Each row is an array of content.
 */
function uikit_view_default(&$variables) {
  $view = $variables['view'];
  $rows = $variables['rows'];
  $options = $view->style_plugin->options;

  // Create attributes for accordion.
  $variables['attributes'] = new Attribute();
  $variables['attributes']['id'] = UIkitViews::getUniqueId($view);

  if (!empty($options['animation']['action']) && $options['animation']['action']) {
    $animation_rand = [
      'uk-animation-fade',
      'uk-animation-scale-up',
      'uk-animation-scale-down',
      'uk-animation-slide-top',
      'uk-animation-slide-top-small',
      'uk-animation-slide-top-medium',
      'uk-animation-slide-bottom',
      'uk-animation-slide-bottom-small',
      'uk-animation-slide-bottom-medium',
      'uk-animation-slide-left',
      'uk-animation-slide-left-small',
      'uk-animation-slide-left-medium',
      'uk-animation-slide-right',
      'uk-animation-slide-right-small',
      'uk-animation-slide-right-medium',
      'uk-animation-shake',
    ];

    $animations = [];
    $animations[] = !empty($options['animation']['reverse']) ? 'uk-animation-reverse' : '';
    $animations[] = !empty($options['animation']['fast']) ? 'uk-animation-fast' : '';
    $animations[] = !empty($options['animation']['origin']) ? $options['animation']['origin'] : '';
    $animations[] = !empty($options['animation']['toggle']) ? 'uk-animation-toggle':'';

    if ($options['animation']['action'] == 'uk-animation-rand') {
      $animations[] = array_rand(array_flip($animation_rand), 1);
    }elseif($options['animation']['action'] != 'uk-animation-rand'){
      $animations[] = $options['animation']['action'];
    }

    if ($options['animation']['position'] == 'views') {
      $variables['attributes']->addClass(implode(' ', array_filter($animations)));
      $variables['animation_views'] = TRUE;
    }
  }

  if (!empty($options['animation']['toggle']) || !empty($options['scrollspy']['toggle'])) {
    $variables['animation_toggle'] = TRUE;
  }
  else {
    $variables['animation_toggle'] = FALSE;
  }

  $default_row_class = [];
  $default_row_class[] = !empty($options['default_row_class']) ? $options['default_row_class'] : '';

  foreach ($rows as $id => $row) {
    $variables['rows'][$id] = [];
    $variables['rows'][$id]['content'] = $row;
    $variables['rows'][$id]['attributes'] = new Attribute();
    $default_row_class[] = !empty($view->style_plugin->getRowClass($id)) ? $view->style_plugin->getRowClass($id) : '';
    $variables['rows'][$id]['attributes']->addClass(implode(' ', array_filter($default_row_class)));

    if (!empty($options['animation']['position']) && $options['animation']['position'] == 'row') {
      if (!empty($options['animation']['toggle']) && $options['animation']['toggle']) {
        $variables['rows'][$id]['attributes']->setAttribute('tabindex', 0);
        $variables['rows'][$id]['animation_row_attributes'] = new Attribute();
        $variables['rows'][$id]['animation_row_attributes']->addClass(implode(' ', array_filter($animations)));
      }
      else {
        $variables['rows'][$id]['attributes']->addClass(implode(' ', array_filter($animations)));
      }
    }
  }

  if (!empty($options['classes'])) {
    $classes = explode(' ', preg_replace('/[^a-zA-Z0-9- ]/', '-', $options['classes']));
    foreach ($classes as $class) {
      $variables['attributes']->addClass($class);
    }
  }

  if (!empty($options['scrollspy']['cls']) && $options['scrollspy']['cls']) {
    $scrollspy = [];
    $scrollspy[] = !empty($options['scrollspy']['cls']) ? 'cls:' . $options['scrollspy']['cls'] : '';
    $scrollspy[] = empty($options['scrollspy']['hidden']) ? 'hidden:false' : '';
    $scrollspy[] = !empty($options['scrollspy']['offset-top']) ? 'offset-top:' . $options['scrollspy']['offset-top'] : '';
    $scrollspy[] = !empty($options['scrollspy']['offset-left']) ? 'offset-left:' . $options['scrollspy']['offset-left'] : '';
    $scrollspy[] = !empty($options['scrollspy']['repeat']) ? 'repeat:true' : '';
    $scrollspy[] = !empty($options['scrollspy']['delay']) ? 'delay:' . $options['scrollspy']['delay'] : '';
    $scrollspy[] = !empty($options['scrollspy']['target']) ? 'target:' . $options['scrollspy']['target'] : '';
    $variables['attributes']->setAttribute('uk-scrollspy', implode(';', array_filter($scrollspy)));
  }
}

/**
 * Prepares variables for UIkit Accordion templates.
 *
 * Default template: uikit-view-accordion.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_accordion(array &$variables) {
  uikit_view_default($variables);

  $view = $variables['view'];
  $options = $view->style_plugin->options;
  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id]['title'] = !empty($options['title_field']) ? $view->style_plugin->getField($id, $options['title_field']) : '';
  }

  $accordion = [];
  $accordion[] = 'targets:' . ($options['targets'] ? $options['targets'] : '> *');
  $accordion[] = 'active:' . (!empty($options['active']) ? $options['active'] : 'false');
  $accordion[] = 'collapsible:' . ($options['collapsible'] ? 'true' : 'false');
  $accordion[] = 'multiple:' . ($options['multiple'] ? 'true' : 'false');
  $accordion[] = 'animation:' . ($options['animation'] ? 'true' : 'false');
  $accordion[] = 'transition:' . $options['transition'];
  $accordion[] = 'duration:' . $options['duration'];

  $list_class = [];
  if (!empty($options['list']) && $options['list']) {
    $list_class[] = 'uk-list';
    foreach ($options['list'] as $list) {
      $list_class[] = $list;
    }
  }

  $variables['attributes']->addClass(implode(' ', array_filter($list_class)));
  $variables['attributes']->setAttribute('uk-accordion', implode(';', array_filter($accordion)));
}

/**
 * Prepares variables for UIkit card templates.
 *
 * Default template: uikit-view-card.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_card(array &$variables) {
  template_preprocess_uikit_view_grid($variables);

  $view = $variables['view'];
  $options = $view->style_plugin->options;

  $media = $options['cards']['type'] == 'media' ? TRUE : FALSE;
  $position = $options['cards']['media']['position'];


  $card_classes = [
    'uk-card',
    $options['cards']['style'] ? $options['cards']['style'] : '',
    $options['cards']['hover'] ? 'uk-card-hover' : '',
    $options['cards']['size'] ? $options['cards']['size'] : '',
  ];

  if ($media && $position == 'left' || $media && $position == 'right') {
    $variables['#attached']['library'][] = 'uikit_views/uikit-view-media';
    $card_classes[] = 'uk-child-width-1-2@s';
    $card_classes[] = 'uk-grid-collapse';
  }

  //
  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id]['header'] = !empty($options['cards']['header']) ? $view->style_plugin->getField($id, $options['cards']['header']) : '';
    if (empty($media) && $options['cards']['card']['footer']) {
      $variables['rows'][$id]['footer'] = $view->style_plugin->getField($id, $options['cards']['card']['footer']);
    }
    if (empty($media) && $options['cards']['card']['badge']) {
      $variables['rows'][$id]['badge'] = $view->style_plugin->getField($id, $options['cards']['card']['badge']);
    }
    if ($position == 'left' || $position == 'right') {
      $variables['rows'][$id]['attributes']->setAttribute('uk-grid', '');;
    }
    $variables['rows'][$id]['attributes']->addClass(implode(' ', array_filter($card_classes)));
  }
  //
  $position_class = [
    $position == 'right' ? 'uk-flex-last@s' : '',
    'uk-card-media-' . $position,
  ];

  $variables['position_class'] = implode(' ', array_filter($position_class));
  $variables['media'] = $media;
  $variables['position'] = $position;
}

/**
 * Prepares variables for UIkit Filter templates.
 *
 * Default template: uikit-view-filter.html.twig.
 *
 * @param array $variables
 *   An filter array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_filter(array &$variables) {
  template_preprocess_uikit_view_card($variables);

  $view = $variables['view'];
  $options = $view->style_plugin->options;
  $view_id = $view->storage->id() . '-' . strtr($view->current_display, '_', '-');

  $titles = [];
  foreach ($view->result as $id => $row) {
    $control_id = !empty($options['filter']['control']) ? $view_id . '-filter-' . $view->style_plugin->getFieldValue($id, $options['filter']['control']) : '';
    $variables['rows'][$id]['filter_control'] = $control_id;
    $titles[$control_id] = $view->style_plugin->getField($id, $options['filter']['control']);
    $variables['rows'][$id]['attributes']->addClass($control_id);
  }

  $variables['filter_title'] = $titles;

  $variables['filter_attributes'] = new Attribute();
  $variables['filter_attributes']['id'] = UIkitViews::getUniqueId($view);
  $variables['filter_attributes']->setAttribute('uk-filter', 'target: ' . $options['filter']['target']);

  $variables['attributes']->removeAttribute('id');
  $variables['attributes']->addClass(ltrim($options['filter']['target'], '.'));
}

/**
 * Prepares variables for UIkit Grid templates.
 *
 * Default template: uikit-view-grid.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_grid(array &$variables) {
  uikit_view_default($variables);

  $view = $variables['view'];
  $options = $view->style_plugin->options;

  $grid = [];
  $grid[] = $options['uikit_grid']['masonry'] ? 'masonry: true' : '';
  $grid[] = $options['uikit_grid']['parallax'] ? 'parallax:' . $options['uikit_grid']['parallax'] : '';

  $variables['attributes']->setAttribute('uk-grid', trim(implode(';', array_filter($grid)), ';'));

  // Create class for grid.
  $grid_class = [
    $options['uikit_grid']['divider'] ? 'uk-grid-divider' : '',
    $options['uikit_grid']['match_height'] ? 'uk-grid-match' : '',
    $options['uikit_grid']['gutter'] ? $options['uikit_grid']['gutter'] : '',
    $options['uikit_grid']['row'] ? $options['uikit_grid']['row'] : '',
    $options['uikit_grid']['column'] ? $options['uikit_grid']['column'] : '',
  ];
  if (!empty($options['flex']) && $options['flex']) {
    $grid_class[] = $options['flex']['horizontal'] ? $options['flex']['horizontal'] : '';
    $grid_class[] = $options['flex']['vertical'] ? $options['flex']['vertical'] : '';
  }
  foreach (['@s', '@m', '@l', '@xl'] as $size) {
    $grid_class[] = $options['uikit_grid']["width_" . $size];
  }

  foreach ($grid_class as $class) {
    $variables['attributes']->addClass($class);
  }

}

/**
 * Prepares variables for UIkit lightbox templates.
 *
 * Default template: uikit-view-lightbox.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_lightbox(array &$variables) {
  template_preprocess_uikit_view_grid($variables);
  $view = $variables['view'];
  $options = $view->style_plugin->options;

  foreach ($variables['rows'] as $id => $row) {
    $original_field = $view->style_plugin->getField($id, $options['lightbox']['original']);
    $thumbnail_field = $view->style_plugin->getField($id, $options['lightbox']['thumbnail']);

    preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i', $original_field, $original_path);
    preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i', $thumbnail_field, $thumbnail_path);

    $variables['rows'][$id]['original_path'] = !empty($original_path[1]) ? $original_path[1] : '';
    $variables['rows'][$id]['thumbnail_path'] = !empty($thumbnail_path[1]) ? $thumbnail_path[1] : '';
    $variables['rows'][$id]['caption'] = !empty($options['lightbox']['caption']) ? $view->style_plugin->getFieldValue($id, $options['lightbox']['caption']) : '';
  }

  $variables['type'] = $options['lightbox']['type'];
  $variables['toggle'] = $options['lightbox']['toggle'];

  $lightbox = [
    'animation:' . $options['lightbox']['animation'],
    'autoplay:' . $options['lightbox']['autoplay'],
    'autoplay-interval:' . $options['lightbox']['autoplay_interval'],
    'pause-on-hover:' . (!empty($options['lightbox']['pause_on_hover']) ? 'true' : ''),
    'toggle:' . $options['lightbox']['toggle'],
    'index:' . $options['lightbox']['index'],
    'video-autoplay:' . ($options['lightbox']['video_autoplay'] ? 'true' : ''),
  ];
  $variables['attributes']->setAttribute('uk-lightbox', implode(';', array_filter($lightbox)));

}

/**
 * Prepares variables for UIkit List templates.
 *
 * Default template: uikit-view-list.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_list(array &$variables) {
  uikit_view_default($variables);
  $view = $variables['view'];
  $options = $view->style_plugin->options;

  // Set the list attributes.
  $variables['list_attributes'] = new Attribute();
  $classes = array_merge($options['list'], ['uk-list']);
  foreach ($classes as $class) {
    if (!empty($class) && $class) {
      $variables['list_attributes']->addClass($class);
    }
  }

  $variables['#attached']['library'][] = 'uikit_views/uikit-view-list';
}

/**
 * Prepares variables for UIkit slider templates.
 *
 * Default template: uikit-view-slider.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_slider(array &$variables) {
  template_preprocess_uikit_view_card($variables);
  $view = $variables['view'];
  $options = $view->style_plugin->options;
  $views_id = $view->storage->id() . '-' . $view->current_display;

  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id]['attributes']->addClass('uk-margin-medium-bottom');
  }

  $slider = [];
  // Create attributes for slider.
  $slider[] = $options['slider']['center'] ? 'center:true' : '';
  if (!empty($options['slider']['autoplay']) && $options['slider']['autoplay']) {
    $slider[] = $options['slider']['autoplay'] ? 'autoplay:true' : '';
    $slider[] = 'autoplay-interval:' . ($options['slider']['autoplay_interval'] ? $options['slider']['autoplay_interval'] : '7000');
  }

  $slider[] = !empty($options['slider']['finite']) ? 'finite: true' : '';
  $slider[] = $options['slider']['sets'] ? 'sets: true' : '';
  $slider[] = empty($options['slider']['pause_on_hover']) ? 'pause-on-hover:false' : '';
  $slider[] = empty($options['slider']['draggable']) ? 'draggable:false' : '';
  $slider[] = 'index:' . $options['slider']['index'];
  $slider[] = 'velocity:' . $options['slider']['velocity'];

  $variables['slider_attributes'] = new Attribute();
  $variables['slider_attributes']['id'] = UIkitViews::getUniqueId($view);
  $variables['slider_attributes']->addClass(strtr($views_id, '_', '-'));
  $variables['slider_attributes']->setAttribute('uk-slider', implode(';', array_filter($slider)));

  $variables['attributes']->addClass('uk-slider-items');
  $variables['attributes']->removeAttribute('id');

  $variables['navigation'] = $options['slider']['navigation'];
  $variables['inverse'] = $options['slider']['inverse'];
  $variables['center'] = $options['slider']['center'];
  $variables['dotnav'] = $options['slider']['dotnav'];
}

/**
 * Prepares variables for UIkit slideshow templates.
 *
 * Default template: uikit-view-slideshow.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_slideshow(array &$variables) {
  uikit_view_default($variables);

  $view = $variables['view'];
  $options = $view->style_plugin->options;

  $slideshow = [];
  // Create attributes for slideshow.
  $slideshow[] = 'animation:' . $options['slideshow']['animation'];
  $slideshow[] = 'autoplay:' . ($options['slideshow']['autoplay'] ? 'true' : 'false');
  $slideshow[] = 'autoplay-interval:' . $options['slideshow']['autoplay_interval'];
  $slideshow[] = $options['slideshow']['finite'] ? 'finite:true' : '';
  $slideshow[] = 'pause-on-hover:' . ($options['slideshow']['pause_on_hover'] ? 'true' : 'false');
  $slideshow[] = 'draggable:' . ($options['slideshow']['draggable'] ? 'true' : 'false');
  $slideshow[] = $options['slideshow']['index'] ? 'index:' . $options['slideshow']['index'] : '';
  $slideshow[] = 'velocity:' . $options['slideshow']['velocity'];
  $slideshow[] = 'ratio:' . $options['slideshow']['ratio'];

  $slideshow[] = !empty($options['slideshow']['min_height']) ? 'min_height:' . $options['slideshow']['min_height'] : '';
  $slideshow[] = !empty($options['slideshow']['max_height']) ? 'max_height:' . $options['slideshow']['max_height'] : '';
  $slideshow[] = !empty($options['slideshow']['easing']) ? 'easing:ease' : '';

  $variables['attributes']->setAttribute('uk-slideshow', implode(';', array_filter($slideshow)));

  $slidenav = [];
  $slidenav[] = !empty($options['slideshow']['slidenav']['uk-slidenav-large']) ? 'uk-slidenav-large' : '';
  $slidenav[] = !empty($options['slideshow']['slidenav']['uk-hidden-hover']) ? 'uk-hidden-hover' : '';
  $variables['slidenav'] = implode(' ', array_filter($slidenav));

  $variables['inverse'] = $options['slideshow']['inverse'];
  $variables['dotnav'] = $options['slideshow']['dotnav'];

  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id]['thumbnav'] = $view->style_plugin->getField($id, $options['slideshow']['thumbnav']['field']) ?: '';
  }
  $position = $options['slideshow']['thumbnav']['position'];
  $variables['thumbnav_position'] = $position;
  $variables['thumbnav_margin'] = $options['slideshow']['thumbnav']['margin'];
  $variables['thumbnav_vertical'] = $options['slideshow']['thumbnav']['vertical'];
  $variables['#attached']['library'][] = 'uikit_views/uikit-view-slideshow';
}

/**
 * Prepares variables for UIkit switcher templates.
 *
 * Default template: uikit-view-switcher.html.twig.
 *
 * @param array $variables
 *   An switcher array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_switcher(array &$variables) {
  uikit_view_default($variables);

  $view = $variables['view'];
  $options = $view->style_plugin->options;
  $view_id = $view->storage->id() . '-' . strtr($view->current_display, '_', '-');

  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id]['switcher_title'] = !empty($options['switcher']['switcher_title']) ? $view->style_plugin->getField($id, $options['switcher']['switcher_title']) : '';
  }

  $position = $options['switcher']['tab']['position'];
  if (!empty($position) && $position == 'uk-tab-left' || $position == 'uk-tab-right') {
    $variables['attributes']->setAttribute('uk-grid', '');
    $variables['position'] = $position ? $position : 'uk-switcher';
    $connect = empty($options['switcher']['connect']) ? 'connect: .switcher-content-' . $view_id : '';
  }
  else {
    $connect = $options['switcher']['connect'] ? 'connect:' . $options['switcher']['connect'] : '';
  }

  $switcher = [];
  $switcher[] = $connect;
  $switcher[] = $options['switcher']['toggle'] ? 'toggle:' . $options['switcher']['toggle'] : '';
  $switcher[] = $options['switcher']['active'] ? 'active:' . $options['switcher']['active'] : '';
  $switcher[] = empty($options['switcher']['swiping']) ? 'swiping:false' : '';
  if (!empty($options['switcher']['animation']) && $options['switcher']['animation']) {
    $switcher[] = $options['switcher']['animation'] ? 'animation:' . implode(',', array_filter($options['switcher']['animation'])) : '';
    $switcher[] = $options['switcher']['duration'] ? 'duration:' . $options['switcher']['duration'] : '';
  }

  $switcher_nav = [];
  if (!empty($options['switcher']['nav']) && $options['switcher']['nav'] == 'subnav') {
    $switcher_nav[] = 'uk-subnav';
    $switcher_nav[] = $options['switcher']['subnav']['type'];
    $switcher_attribute = 'uk-switcher';
  }
  else {
    $switcher_attribute = 'uk-tab';
    $switcher_nav[] = $position;
  }
  $switcher_nav[] = $options['switcher']['alignment'];

  $variables['switcher_nav_attributes'] = new Attribute();
  $variables['switcher_nav_attributes']->setAttribute($switcher_attribute, implode(';', array_filter($switcher)));
  $variables['switcher_nav_attributes']->addClass(implode(' ', array_filter($switcher_nav)));

  $switcher_content = [];
  $switcher_content[] = $options['switcher']['connect'] ? $options['switcher']['connect'] : 'uk-switcher';
  $switcher_content[] = 'switcher-content-' . $view_id;
  $variables['switcher_content_attributes'] = new Attribute();
  $variables['switcher_content_attributes']->addClass(implode(' ', array_filter($switcher_content)));

}

/**
 * Prepares variables for UIkit Table templates.
 *
 * Default template: uikit-view-table.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_uikit_view_table(array &$variables) {
  template_preprocess_views_view_table($variables);
  $view = $variables['view'];
  $options = $view->style_plugin->options;
  $variables['overflow_auto'] = $options['overflow_auto'];
  $variables['attributes'] = new Attribute();
  $variables['attributes']['id'] = UIkitViews::getUniqueId($view);
  $table_size[] = $options['table_size'];
  $classes = array_merge($options['table_modifiers'], $table_size, ['uk-table']);
  $variables['attributes']->addClass(implode(' ', array_filter($classes)));
  foreach ($options['header_field'] as $key => $field) {
    $variables['header_class'][$key] = $field;
  }
}

/**
 * Prepares variables for UIkit transition templates.
 *
 * Default template: uikit-view-transition.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 *   - icon: To display an overlay icon.
 */
function template_preprocess_uikit_view_transition(array &$variables) {
  template_preprocess_uikit_view_grid($variables);
  $view = $variables['view'];
  $options = $view->style_plugin->options;
  $variables['icon'] = !empty($options['transition']['icon']) ? $options['transition']['icon'] : '';

  $transition = [];
  $transition[] = !empty($options['transition']['effects']) ? $options['transition']['effects'] : '';
  $transition[] = !empty($options['transition']['position']) ? $options['transition']['position'] : '';
  $transition[] = !empty($options['transition']['transition_margin']) ? $options['transition']['transition_margin'] : '';
  $transition[] = !empty($options['transition']['inverse']) ? $options['transition']['inverse'] : '';
  $transition[] = !empty($options['transition']['overlay']) ? $options['transition']['overlay'] : '';
  $content_class = explode(',', trim($options['transition']['content_class']));

  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id]['thumbnail'] = $view->style_plugin->getField($id, $options['transition']['thumbnail']) ?: '';
    if (!empty($options['transition']['thumbnail_second']) && $thumbnail_second = $view->style_plugin->getField($id, $options['transition']['thumbnail_second'])) {
      preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i', $thumbnail_second, $thumbnail_second_path);
      $variables['rows'][$id]['thumbnail_second'] = !empty($thumbnail_second_path[1]) ? $thumbnail_second_path[1] : '';
    }
    $variables['rows'][$id]['transition_attributes'] = new Attribute();
    $variables['rows'][$id]['transition_attributes']->addClass(implode(' ', array_filter(array_merge($transition, $content_class))));
  }
}
